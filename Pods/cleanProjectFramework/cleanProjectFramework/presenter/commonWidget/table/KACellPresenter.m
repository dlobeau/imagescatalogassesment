//
//  KACellPresenter.m
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 20/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KACellPresenter.h"
#import "KACellSwipeAction.h"

@interface KACellPresenter()

@property NSMutableArray<id<KACellSwipeAction>>* swipLeftEvents;

@end

@implementation KACellPresenter

@synthesize swipLeftEvents = _swipLeftEvents;

+(NSString *) getTypeTag
{
    return @"cell";
}

-(NSArray<id<KACellSwipeAction>>*) swipLeftEvents
{
    if(_swipLeftEvents == nil)
    {
        NSArray<id<KAPresenter>> * List = [self childPresenter];
        for(int i = 0; i< [List count]; i++)
        {
            id<KACellSwipeAction> CurrentAction = (id<KACellSwipeAction>)[List objectAtIndex:i];
            if([CurrentAction conformsToProtocol:@protocol(KACellSwipeAction)])
            {
                if(_swipLeftEvents == nil)
                {
                    _swipLeftEvents = [[NSMutableArray alloc] init];
                }
                [_swipLeftEvents addObject:CurrentAction];
            }
        }
    }
    return _swipLeftEvents;
}

-(void) setSwipLeftEvents:(NSMutableArray<id<KACellSwipeAction>> *)swipLeftEvents
{
    _swipLeftEvents = swipLeftEvents;
}
@end
