//
//  KATable.h
//  taskList
//
//  Created by Didier Lobeau on 28/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"
#import "KASection.h"
NS_ASSUME_NONNULL_BEGIN



@protocol KATable <KAPresenter>

-(NSArray<id<KASection>> *) sections;
-(id<KASection>) sectionAtIndex:(NSInteger) Index;

-(NSSet<NSString *> *) getHeaderTypeList;

-(NSArray<id<KACell>> *) allCells;
@end

NS_ASSUME_NONNULL_END
