//
//  KASectionPresenter.m
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 20/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KASectionPresenter.h"

@implementation KASectionPresenter

+(NSString *) getTypeTag
{
    return @"section";
}

-(BOOL) isInitializedOnParentCreation
{
    return NO;
}

-(NSArray<id<KACell>> *) cells
{
    NSMutableArray<id<KACell>> *ReturnValue = nil;
    
    NSArray<id<KACell>> *allCHilds = (NSArray<id<KACell>> *)[self childPresenter];
    
    for(int i = 0; i< allCHilds.count; i++)
    {
        id<KACell> CurrentCell = [allCHilds objectAtIndex:i];
        if([CurrentCell conformsToProtocol: @protocol(KACell) ])
        {
            if(ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            [ReturnValue addObject:CurrentCell];
        }
        
    }
    return ReturnValue;
    
}

-(id<KACell>) cellAtIndex:(NSInteger) Index
{
    NSArray<id<KACell>> *Cells = self.cells;
    NSAssert(Index< Cells.count,@"index: %ld can't be bigger than cell list: %ld",Index,Cells.count);
    
    return [Cells objectAtIndex:Index];
}

@end
