//
//  KACell.h
//  taskList
//
//  Created by Didier Lobeau on 28/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@protocol KACellSwipeAction;

@protocol KACell <KAPresenter>

-(NSArray<id<KACellSwipeAction>>*) swipLeftEvents;

@end

NS_ASSUME_NONNULL_END
