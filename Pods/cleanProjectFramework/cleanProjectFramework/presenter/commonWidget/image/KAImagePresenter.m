//
//  KAImagePresenter.m
//  kakebo
//
//  Created by Didier Lobeau on 12/06/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KAImagePresenter.h"

@implementation KAImagePresenter

+(NSString * )getTypeTag
{
    return @"image";
}

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
     return self;
}

@end
