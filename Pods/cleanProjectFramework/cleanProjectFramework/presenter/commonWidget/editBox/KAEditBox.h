//
//  KAEditBox.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 15/11/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

#import "KAPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@protocol KAEditBox <KAPresenter>

@end

NS_ASSUME_NONNULL_END
