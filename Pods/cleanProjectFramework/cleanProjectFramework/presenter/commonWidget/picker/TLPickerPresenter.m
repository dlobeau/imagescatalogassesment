//
//  TLPickerPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLPickerPresenter.h"
#import "KADate.h"


@implementation TLPickerPresenter

-(NSDate *) date
{
    id<KADate> Date = (id<KADate>) self.data;
       
    return Date.getDateObject;
}

+(NSString *) getTypeTag
{
    return @"datePicker";
}




@end
