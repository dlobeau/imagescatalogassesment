//
//  KACommandGeneric.m
//  kakebo
//
//  Created by Didier Lobeau on 13/03/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KACommandGeneric.h"
#import "KADomain.h"
@interface KACommandGeneric()



@end

@implementation KACommandGeneric

-(id) initWithOwner:(id<KAPresenter>)Owner
{
    self = [super init];
    
    self.owner = Owner;
    
    return self;
}

-(void) doCommand
{
    
}


@end
