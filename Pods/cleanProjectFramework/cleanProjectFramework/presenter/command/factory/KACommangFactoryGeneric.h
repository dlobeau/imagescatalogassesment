//
//  KACommangFactoryGeneric.h
//  taskList
//
//  Created by Didier Lobeau on 10/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KACommandFactory.h"

NS_ASSUME_NONNULL_BEGIN

@interface KACommangFactoryGeneric : NSObject<KACommandFactory>

@end

NS_ASSUME_NONNULL_END
