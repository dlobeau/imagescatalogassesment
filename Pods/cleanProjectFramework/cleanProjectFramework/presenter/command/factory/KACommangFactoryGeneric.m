//
//  KACommangFactoryGeneric.m
//  taskList
//
//  Created by Didier Lobeau on 10/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACommangFactoryGeneric.h"
#import "KACommandChangeDataLabel.h"
#import "KACommandChangeData.h"
#import "KCommandChangeDate.h"
#import "KACommandChangeSelfData.h"
#import "KACommandCHangeBooleanValue.h"

@implementation KACommangFactoryGeneric

-(id<KACommand>) createCommandOnLabelModificationWithNewLabel:(NSString *) NewLabel WithSender:(id<KAPresenter>) Sender
{
    return [[KACommandChangeDataLabel alloc] initWithOwner:Sender WithNewLabel:NewLabel];
}
-(id<KACommand>) createCommandOnChildPresenterModification:(id<KAPresenter>) NewSelection WithSender:(id<KAPresenter>) Sender
{
    return [[KACommandChangeData alloc] initWithOwner:Sender WithNewChildSelected:NewSelection];
}
-(id<KACommand>) createCommandOnPresenterModificationWithNewDate:(NSDate *) NewDate WithSender:(id<KAPresenter>) Sender
{
    return [[KCommandChangeDate alloc] initWithOwner:Sender WithNewDate:NewDate];
}

-(id<KACommand>) createCommandOnPresenterModificationWithPresenterWithNewValue:(id<KAPresenter>) PresenterWithNewValue WithSender:(id<KAPresenter>) Sender
{
    return [[KACommandChangeSelfData alloc] initWithOwner:Sender WithPresenter:PresenterWithNewValue];
}

-(id<KACommand>) createCommandOnPresenterModificationWithBoolean:(BOOL) BooleanValue WithSender:(id<KAPresenter>) Sender
{
    return [[KACommandCHangeBooleanValue alloc] initWithOwner:Sender WithBooleanValue:BooleanValue];
}

@end
