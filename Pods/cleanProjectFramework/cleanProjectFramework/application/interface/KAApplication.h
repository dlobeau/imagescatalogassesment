//
//  KAApplication.h
//  kakebo
//
//  Created by Didier Lobeau on 22/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//


#import "KAPresenter.h"
#import "KATreeNavigable.h"

@protocol KADomain;
@protocol TLTasksDataBase;
@protocol KACalendar;
@protocol KASerializeObjectFactory;

@protocol KAApplication <KAPresenter,KATreeNavigable>



-(id<KAPresenter>) window;

-(void) start;

-(id<KACalendar>) calendar;

-(id<KASerializeObjectFactory>) factory;
-(void) setFactory:(id<KASerializeObjectFactory>) factory;

-(NSDictionary *) getWidgetTagList;


@end
