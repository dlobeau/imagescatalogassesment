//
//  KACalendarObjectSerializable.m
//  kakebo
//
//  Created by Didier Lobeau on 27/02/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KACalendarGeneric.h"

#import "KADateAttribute.h"

#import "KASerializeObjectFactory.h"
#import "KANullDateGeneric.h"


@interface KACalendarGeneric()


@property  NSDateFormatter * dateFormat ;
@property id<KADate,KASeriazableObject> NULL_DATE;

@property NSLocale *usedLocal;
@end


@implementation KACalendarGeneric

@synthesize  dateFormat = _dateFormat ;
@synthesize  usedLocal = _usedLocal ;
@synthesize  NULL_DATE = _NULL_DATE;


+(NSString *) getTypeTag
{
    return @"calendar";
}

-(NSLocale *) usedLocal
{
    if(_usedLocal == nil)
    {
        _usedLocal = [NSLocale localeWithLocaleIdentifier:[[NSLocale preferredLanguages] firstObject]];
    }
    return _usedLocal;
}
-(void) setUsedLocal:(NSLocale *)usedLocal
{
    _usedLocal = usedLocal;
}



-(id<KADate>) createCurrentDay
{
    return [self createDayWithDateObject:[NSDate date]];
   
}

-(id<KADate>) createDayWithString:(NSString *) strDay
{
    id<KADate,KASeriazableObject> Date = [self.factoryDelegate createObjectFromFamilyType:KADateAttribute.getTypeTag];
    Date.label = strDay;
    Date.labelIdentifier = strDay.copy;
    Date.ID = @"date";
    Date.factoryDelegate = self.factoryDelegate;
    Date.calendar = self;
    return Date;
    
}

-(id<KADate>) createDayWithDateObject:(NSDate *) DateObject
{
    NSString * strDate =[[self getDateFormat] stringFromDate:DateObject];
    id<KADate> Date = [self createDayWithString:strDate];
    
    Date.calendar = self;
    
    return Date;
}

-  (NSDateFormatter *) getDateFormat
{
    if( _dateFormat == nil )
    {
        _dateFormat = [[NSDateFormatter alloc] init];
        _dateFormat.locale = self.usedLocal;
        [_dateFormat setDateFormat:@"yyyy-MM-dd"];
        
    }
    return _dateFormat;
}

-(id<KADate>) NULL_DATE
{
    if(_NULL_DATE == nil)
    {
        _NULL_DATE = [self.factoryDelegate createObjectFromFamilyType:KANullDateGeneric.getTypeTag];
        _NULL_DATE.label = KANullDateGeneric.getTypeTag;
        _NULL_DATE.labelIdentifier = KANullDateGeneric.getTypeTag.copy;
        _NULL_DATE.ID = @"date";
        _NULL_DATE.factoryDelegate = self.factoryDelegate;
        _NULL_DATE.calendar = self;
        
    }
    return _NULL_DATE;
}
-(void) setNULL_DATE:(id<KADate,KASeriazableObject>)NULL_DATE
{
    _NULL_DATE = NULL_DATE;
}
@end
