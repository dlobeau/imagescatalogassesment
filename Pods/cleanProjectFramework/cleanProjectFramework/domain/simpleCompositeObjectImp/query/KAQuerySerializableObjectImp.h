//
//  KAQuerySerializableObjectImp.h
//  kakebo
//
//  Created by Didier Lobeau on 09/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//


#import "KAQuery.h"
#import "KASeriazableObjectTableImp.h"



@interface KAQuerySerializableObjectImp : KASeriazableObjectTableImp<KAQuery>

@end
