//
//  KACriteriaSerializableObjectImp.m
//  kakebo
//
//  Created by Didier Lobeau on 09/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//



#import "KACriteriaSerializableObjectImp.h"

#import "KAQueryBusinessUseCase.h"
#import "KADomain.h"


@interface KACriteriaSerializableObjectImp()

@property id<KASeriazableObject> serializableObject;
@property id<KADomain> context;
@property id<KADomain> value;

@end

@implementation KACriteriaSerializableObjectImp

@synthesize context = _context;
@synthesize value = _value;

+(NSString *) getTypeTag
{
    return @"criteria";
}

-(id<KAQueryBusinessUseCase>) queryBusinessUseCase
{
    return (id<KAQueryBusinessUseCase>)[self getChildwithTypeId:@"useCase"];;
}

-(id<KADomain>) field
{
    return (id<KADomain>)[self getChildwithTypeId:@"field"];;
}
-(id<KADomain>) value
{
    _value = (id<KADomain>)[self getChildwithTypeId:@"value"];
        
    return _value;
}

-(void) setValue:(id<KADomain>) Value
{
    _value = Value;
}





@end
