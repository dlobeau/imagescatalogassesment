//
//  KABooleanAttribute.h
//  kakebo
//
//  Created by Didier Lobeau on 20/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KAGenericDomain.h"
#import "KABoolean.h"

//bool injected
@interface KABooleanAttribute : KAGenericDomain<KABoolean>

-(id) initWithBoolValue:(BOOL ) Value WithTypeId:(NSString * )TypeId;
-(id) initWithBoolValue:(BOOL ) Value;

@end
