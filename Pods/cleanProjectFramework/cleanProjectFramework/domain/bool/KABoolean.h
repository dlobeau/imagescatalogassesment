//
//  KABoolean.h
//  kakebo
//
//  Created by Didier Lobeau on 20/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//


#import "KADomain.h"
@protocol KABoolean <KADomain>

-(BOOL) value;
-(void) setValue:(BOOL) NewValue;



@end
