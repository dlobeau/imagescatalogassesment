//
//  KACreateUIWidgetCollection.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KACreateUIWidgetGenericFactory.h"

NS_ASSUME_NONNULL_BEGIN

@interface KACreateUIWidgetCollection : KACreateUIWidgetGenericFactory

@end

NS_ASSUME_NONNULL_END
