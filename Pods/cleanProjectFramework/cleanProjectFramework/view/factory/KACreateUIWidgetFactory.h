//
//  createUIWidgetFactory.h
//  kakebo
//
//  Created by Didier Lobeau on 07/10/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KAView;
@protocol KAPresenter ;


@protocol KACreateUIWidgetFactory 

-(id<KAView> ) createWithOwner:(id<KAView>) Owner WithWidgetType:(NSString * ) WidgetType WithWidgetIdentifier:(NSString *) identifier WithSender:(id<KAPresenter>) Sender;


-(id<KAView>) getChildWithID:(NSString *) ID WithWidgetParent:(id<KAView>) WidgetParent;

@end
