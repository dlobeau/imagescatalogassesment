//
//  TLUIDatePickerFacade.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIDatePickerFacade.h"
#import "TLPicker.h"
#import "KADelegatePushNextView.h"

@interface TLUIDatePickerFacade()

@property (weak) id<TLPicker> interface;
@property (weak) id<KADelegatePushNextView> delegatePushNextView;

@end

@implementation TLUIDatePickerFacade

-(UIView *) view
{
    return self;
}

-(void) setContent
{
    [self setDate:self.interface.date];
   
}

-(BOOL) validateUserChange
{
    NSDate *NewDate = [self date];
    [self.interface addCommandOnPresenterModificationWithNewDate:NewDate WithSender:self.interface];
    [self.interface executeCommandHierarchy];
    return NO;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return YES;
}

- (NSString *)widgetID
{
    return self.restorationIdentifier;
}



@end
