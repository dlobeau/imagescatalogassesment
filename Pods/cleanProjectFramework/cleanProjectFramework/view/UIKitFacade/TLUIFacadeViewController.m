//
//  TLUIFacadeViewController.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIFacadeViewController.h"
#import "KAPresenter.h"

@interface TLUIFacadeViewController ()

@property id<KADelegatePushNextView> delegatePushNextView;
@property (weak) id<KAPresenter> interface;

@end

@implementation TLUIFacadeViewController

@synthesize delegatePushNextView = _delegatePushNextView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(id) view
{
    return super.view;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return NO;
}

-(NSString *) widgetID
{
    return nil;
}

-(void) setContent
{
    
}


-(BOOL) validateUserChange
{
    return NO;
}

-(void) pushNextPage:(TLUIFacadeViewController *) nextPage
{
    [self.navigationController pushViewController:nextPage animated:YES];
}

- (id<KADelegatePushNextView>) delegatePushNextView
{
    if(_delegatePushNextView == nil)
    {
        _delegatePushNextView = self;
    }
    return _delegatePushNextView;
}
-(void) setDelegatePushNextView:(id<KADelegatePushNextView>)delegatePushNextView
{
    _delegatePushNextView = delegatePushNextView;
}

-(void) closeWindow
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
