//
//  TLUIViewFacade.m
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUIViewFacade.h"

@interface TLUIViewFacade()

@property (weak) id<KADelegatePushNextView> delegatePushNextView;
@property (weak) id<KAPresenter> interface;

@end

@implementation TLUIViewFacade

-(id) view
{
    return self;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return NO;
}

-(NSString *) widgetID
{
    return self.accessibilityIdentifier;
}

-(void) setContent
{
    
}


-(BOOL) validateUserChange
{
    return NO;
}



@end
