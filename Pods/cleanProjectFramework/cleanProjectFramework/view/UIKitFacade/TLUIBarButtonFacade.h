//
//  TLUIBarButtonFacade.h
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAView.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLUIBarButtonFacade : UIBarButtonItem<KAView>

-(void) actionFromOwnerWindow:(id<KAView>) OwnerWindow;


@end

NS_ASSUME_NONNULL_END
