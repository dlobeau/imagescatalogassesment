//
//  TLUIFacadeViewController.h
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAView.h"
#import "KADelegatePushNextView.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLUIFacadeViewController : UIViewController<KAView,KADelegatePushNextView>

-(void) closeWindow;

@end

NS_ASSUME_NONNULL_END
