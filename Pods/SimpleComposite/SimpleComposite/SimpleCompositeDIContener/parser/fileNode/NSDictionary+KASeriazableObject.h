//
//  NSDictionary.h
//  kakebo
//
//  Created by Didier Lobeau on 09/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KASeriazableObject;
@protocol KASerializeObjectFactory;

@interface NSDictionary (KASeriazableObject)

+(id) dictionaryFromString:(NSString *) String;

-(id<KASeriazableObject>) getSeriazableObjectWithFactory:(id<KASerializeObjectFactory>) Factory;

-(NSString *) toString;

@end
