//
//  BVArrayNode.h
//  Skwipy
//
//  Created by Didier Lobeau on 04/02/2016.
//  Copyright (c) 2016 FollowTheDancer. All rights reserved.
//

#import "KADcfGenericNode.h"

@interface KADcfArrayNode : KADcfGenericNode

@property NSArray* array;

-(id) initWithArray:(NSArray *) array  WithNodeName:(NSString *)NodeName;

@end
