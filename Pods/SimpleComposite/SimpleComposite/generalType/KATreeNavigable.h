//
//  KATreeNavigable.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KAIterator;

@protocol KATreeNavigable <NSObject>

-(id<KATreeNavigable>) parent;
-(void) setParent:(id<KATreeNavigable>) Parent;

-(id<KATreeNavigable>) root;

-(id<KATreeNavigable>) nextSibling;

-(BOOL) addChild:(id<KATreeNavigable> ) ChildObject;
-(BOOL) addChild:(id<KATreeNavigable> ) ChildObject WithTypeId:(NSString *) TypeId;
-(BOOL) addAllChild:(NSArray<id<KATreeNavigable>> *) ChildListObject;

-(NSArray<id<KATreeNavigable>> *) getChildListWithGroupId:(NSString *) GroupId;
-(id<KATreeNavigable> ) getChildwithTypeId:(NSString *) TypeId;

-(id<KAIterator>) getIterator;
-(id<KAIterator>) getIteratorWithDynamicChildsWithID:(NSString *) ID;

-(NSString *) path;

@end

NS_ASSUME_NONNULL_END
