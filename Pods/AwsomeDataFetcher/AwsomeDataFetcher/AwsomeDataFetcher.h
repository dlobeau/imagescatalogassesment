//
//  AwsomeDataFetcher.h
//  AwsomeDataFetcher
//
//  Created by Didier Lobeau on 11/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AwsomeDataFetcher.
FOUNDATION_EXPORT double AwsomeDataFetcherVersionNumber;

//! Project version string for AwsomeDataFetcher.
FOUNDATION_EXPORT const unsigned char AwsomeDataFetcherVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AwsomeDataFetcher/PublicHeader.h>


