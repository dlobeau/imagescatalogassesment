//
//  RemoteImageFetcher.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 11/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import Alamofire



public protocol DataFetcherDelegate
{
     func identification()->(String)
    
    func didFetchData(WithFetchedData DataFectched:Any?,WithError ErrorReported:Error?)->()
}

public protocol RemoteDataFetcher
{
    func fetch(WithURL URL:String, WithDelegate Delegate:DataFetcherDelegate)->()
    
    func fetch(WithURL URL:String, WithDelegate Delegate:DataFetcherDelegate, WithLimits Limits:LimitRequestParameter)->()
    
    func cancel(WithURL URL: String, WithDelegate Delegate:DataFetcherDelegate)->()
   
}


public struct LimitRequestParameter
{
    static let SINCE_ID:String = "since_id"
    static let MAX_ID:String = "max_id"
      
    
    let limitHigh:String
    let limitLow:String
    
    public init(WithLimitLow LimitLow:String,WithLimitHigh LimitHigh:String)
    {
        limitHigh = LimitHigh
        limitLow = LimitLow
    }
    public init(_ Limits:(String,String))
    {
        self.init(WithLimitLow: Limits.0,WithLimitHigh:Limits.1)
    }
    
    public func paramaterRequestFormat()->([String:String])
    {
        var ReturnValue = [String:String]()
       
        if(!limitLow.isEmpty)
        {
            ReturnValue[LimitRequestParameter.SINCE_ID] =  limitLow
        }
        if(!limitHigh.isEmpty)
        {
            ReturnValue[LimitRequestParameter.MAX_ID] =  limitHigh
        }
        
        return ReturnValue
    }
    
}

public class RemoteDataFetcherAlamo:RemoteDataFetcher
{
    private var delegate: DataFetcherDelegate?
    private var UrlList = [String]()
    private var currentQueue:[String:DataRequest] = [String:DataRequest]()
   
    private var secureSessionManager:SSLPinningManager?
    
    var delegateList:[DataFetcherDelegate]? = [DataFetcherDelegate]()
  
    private var session:Session?
    
    
    public init(WithCertificationFile CertificationFile:LocalFile, WithKey Key:String)
    {
        secureSessionManager = SSLPinningManager(WithCertificationFilePath: CertificationFile, WithKey: Key)
    }
   
    public init()
    {
        
    }
    
    public func fetch(WithURL URL: String, WithDelegate Delegate:DataFetcherDelegate)
    {
        self.fetch(WithURL: URL, WithDelegate: Delegate, WithLimits:LimitRequestParameter.init(WithLimitLow: "", WithLimitHigh: ""))
    }
    
    
    public func fetch(WithURL URL:String, WithDelegate Delegate:DataFetcherDelegate, WithLimits Limits:LimitRequestParameter)->()
    {
        if let SecuredSessionManagement = secureSessionManager
        {
            session = SecuredSessionManagement.sessionManager!
            
            guard let Session = self.session
            else
            {
                return
            }
            
            let Request = Session.request(
                    URL,
                    method: .get,
                    parameters:Limits.paramaterRequestFormat(),
                    encoder: URLEncodedFormParameterEncoder.default)
                      
            
            let Key = URL + Delegate.identification()
            currentQueue[Key] = Request
            Request.responseData
            {
                response in self.didFetchData(WithFetchedData: response,WithErrorInSession: SecuredSessionManagement.sessionError,
                                                  WithURL: URL,
                                                  WithDelegate: Delegate)
            }
        }
    }
    
    
    
    func didFetchData(WithFetchedData DataFetched:Any?, WithErrorInSession ErrorInSession:Error?,  WithURL URL:String, WithDelegate Delegate:DataFetcherDelegate)->()
    {
        if let DataResponse = DataFetched as? AFDataResponse<Data>
        {
            let Key = URL + Delegate.identification()
            currentQueue.removeValue(forKey: Key)
            
            let DataFetcheError = ErrorInSession != nil ? ErrorInSession : DataResponse.error
            
            Delegate.didFetchData(WithFetchedData: DataResponse.data, WithError:DataFetcheError)
        }
    }
    
    public func cancel(WithURL URL: String, WithDelegate Delegate:DataFetcherDelegate)->()
    {
        let Key = URL + Delegate.identification()
        
         let DataRequestExtracted = currentQueue[Key]
        
         DataRequestExtracted!.cancel()
    }
    
}



