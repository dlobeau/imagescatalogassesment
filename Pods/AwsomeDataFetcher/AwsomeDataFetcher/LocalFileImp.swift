//
//  LocalFile.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 05/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation


public protocol LocalFile
{
    var completeFilePathAndName:String{get}
    
    func fetchContent() throws ->(Any?)
    func cleanFileFromUserEnvironment() throws ->()
    func isFileExist()->(Bool)
    
    func update(WithContent NewContent:Data)->()
    
}



public class LocalFileImp:LocalFile
{
    var fetchedData: Any?
    var identification:String
    {
        return self.completeFilePathAndName
    }
    
    public var completeFilePathAndName:String
    public var fileName:String
    private var bundle:Bundle
    private var fileContent:String?
    
    public init( WithBundle FileBundle:Bundle, WithURL URL:String)
    {
        self.bundle = FileBundle
        
        fileName  = URL
        self.completeFilePathAndName = "\(LocalFileImp.environmentRoot())/\(URL)"
    }
    
    static func  environmentRoot()->(String)
    {
        var ReturnValue = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last
        
        ReturnValue = "\(ReturnValue!)/userApplication";
        
        return ReturnValue!;
    }
    
    public func update(WithContent NewContent:Data)->()
    {
         let CompleteFileName = self.completeFilePathAndName
        
             FileManager.default.createFile(atPath: CompleteFileName, contents: nil)
             let F = FileHandle.init(forWritingAtPath: CompleteFileName)
             F?.write(NewContent)
         
    }
    
    public func fetchContent() throws ->(Any?)
    {
        if (!self.isFileExist())
        {
            try self.copyRessource(WithURL: self.fileName)
        }
        
        let ReturnValue = NSData(contentsOfFile: self.completeFilePathAndName)
        
        return ReturnValue
    }
    
    public func cleanFileFromUserEnvironment() throws ->()
    {
        let fileManager = FileManager.default
        let FileName = self.completeFilePathAndName;
        try fileManager.removeItem(atPath: FileName)
     }

    public func isFileExist()->(Bool)
    {
        var bReturnValue:Bool = true;
        
        let fileManager = FileManager.default
        
        if (!fileManager.fileExists(atPath:self.completeFilePathAndName))
        {
            bReturnValue = false;
        }
        
        return bReturnValue;
    }

    func copyRessource(WithURL URL:String) throws ->()
    {
        let FolderPath = LocalFileImp.environmentRoot()
          
        try self.copyRessourceFile(WithURL: URL, WithFolderDestination: FolderPath)
        
    }
    
    func copyRessourceFile(WithURL URL:String ,WithFolderDestination FolderName:String) throws -> ()
    {
               
        try self.createDirectory(WithFolderPath:FolderName)
        
        let fileManager = FileManager.default
        if (!fileManager.fileExists(atPath: self.completeFilePathAndName))
        {
            let NameAndExtension = self.fileNameAndExtension(WithFileURL: URL)
            
            if let sourcePath = self.bundle.path(forResource: NameAndExtension.0, ofType: NameAndExtension.1)
            {
                let fileManager = FileManager.default
                
                let NewDestination  = "\(FolderName)/\(NameAndExtension.0)\(NameAndExtension.1)"
                
                try fileManager.copyItem(atPath: sourcePath, toPath: NewDestination)
            }
        }
        
        
    }

    func fileNameAndExtension(WithFileURL FileURL:String)->(String,String)
    {
        let DotIndex = FileURL.firstIndex(of: ".") ?? FileURL.endIndex
        let Extention = FileURL.suffix(from: DotIndex)
        let Name = FileURL.prefix(upTo: DotIndex)
               
        return (String(Name),String(Extention))
    }
    

    func createDirectory(  WithFolderPath  FolderPath:String)throws ->()
    {
        let fileManager =  FileManager.default
        var isDirectory:ObjCBool = false
       
        let doesFileExist = fileManager.fileExists(atPath: FolderPath, isDirectory: &isDirectory)
        if (!doesFileExist)
        {
            try fileManager.createDirectory(atPath: FolderPath, withIntermediateDirectories: true, attributes: nil)
        }
    }

}
