Library in Swift for data fetching over network
## Features

*  Data are loaded asynchroneously 
*  Loaded data are cached 
*  The cache  have a configurable max capacity and evict data not recently used;
*  Data loading can be cancelled


## Requirements

- iOS 9.0+ 
- Swift 3.2, 4.x
  - Xcode 9.0+
- CocoaPods 1.1.1+ (if you use)

 ## Installation

AwsomeFileFetcher is available through [CocoaPods](https://cocoapods.org).

### CocoaPods

To install AwsomeFileFetcher with CocoaPods, add the following lines to your `Podfile`.

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '9.0' 
use_frameworks!

pod 'AwsomeDataFetcher'


```

Then run `pod install` command. For details of the installation and usage of CocoaPods, visit [its official website](https://cocoapods.org).

## How to use the library

Check the unit test on the project

## example of project that used this library

https://gitlab.com/dlobeau/awsomeimageloader
