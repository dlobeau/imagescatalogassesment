Application for image loading in pinterest like style 

![image.png](./docs/image.png)

## Features

* One screen application that shows a list containing the items in a remote catalog. 
* All the fields that describe the image are visible in the UI (including the ID).
* When the application is opened a new request is performed and the most recent 10 items are shown. 
* When the user scrolls down past the oldest item another block of 10 items are be appended to the list. 
* When the user scrolls up past the most recent item the application make request the API for newer items. 
* Scrolling up past the beginning of the list trigger a visual effect that indicates that more data is being loaded.
* The application cache the loaded items persistently between application launches. 
* When the application is opened the most recent 10 items from cache are shown.
* SSL pinning is used for each communication with the backend and the app fail and warn the user if necesssary.
* Application is test extensively with unitary and integration test
* The local cache is Encrypt (AES 256) to store the data using a static key.
* The part of the app used for network access is made available through [cocoapod](https://gitlab.com/dlobeau/awsomefilefetcher)


## Clean architecture

This simple image loading app is a project that illustrated the [clean architecture chosen](https://gitlab.com/dlobeau/clean-project-framework-objc). 
Our method deals with the generation of the app, based on XML description language files. 
The parser for the description files and the base classes for the project are available 
through the installation of Pod in your XCODE workspace. This architecture of the app following the clean architecture rules as proposed 
by [Robert C Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) (Uncle bob).
 
This project illustrate how the clean archiecture framework handle multithreading task 

## Dependency

for the clean architecture:
https://gitlab.com/dlobeau/clean-project-framework-objc

for the data fetching:
https://gitlab.com/dlobeau/awsomefilefetcher

[SwiftInject](https://github.com/Swinject/Swinject) for as Dependency Injection container




