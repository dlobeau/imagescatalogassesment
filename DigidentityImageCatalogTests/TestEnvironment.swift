//
//  TestEnvironment.swift
//  DigidentityImageCatalogTests
//
//  Created by Didier Lobeau on 05/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import Swinject
import cleanProjectFramework
import XCTest
import AwsomeDataFetcher

class TestEnvironment:NSObject
{
    var contener:KAInjectionDependencyDelegate
    var applicationFileName:String
    
    fileprivate var applicationVar:KAApplication?
    
    convenience init(WithDiContener DIContener:KAInjectionDependencyDelegate)
    {
        self.init(WithDiContener:DIContener, WithApplicationFileName:"ApplicationMainWindowMainApp")
    }
    
    init(WithDiContener DIContener:KAInjectionDependencyDelegate, WithApplicationFileName ApplicationFileName:String)
    {
        self.contener = DIContener
        self.applicationFileName = ApplicationFileName
    }
    
    var application:KAApplication?
    {
        get
        {
            if(applicationVar == nil)
            {
                //creation of application instance from application declaration file and dependency injection list files
                KAApplicationSerializableObjectImp.createInstance(withFileName: self.applicationFileName, withDIDelegate: self.contener)
                
                guard let ReturnValue =  KAApplicationSerializableObjectImp.instance() as? ApplicationManager
                else
                {
                    return nil
                }
                
                ReturnValue.start();
                try! ReturnValue.loadData()
                
                applicationVar = ReturnValue
            }
            return applicationVar
            
        }
    
    }
    
    var dataBase:CatalogItemsDataBase?
    {
        get
        {
            guard let Appli = application as? ApplicationManager
            else
            {
               return nil
            }
            return  Appli.dataBase
        }
    }
    
    func createWithContentsOfFile(WithFileName FileName:String )->(KAPresenter)
       {
           return application?.factory()?.createWithContentsOfFile(withFileName: FileName) as! KAPresenter
       }
}

class TestApplicationStarted:NSObject, KAASynchronousDataBindingDelegate
{
    var Success = 0
    func didASynchronousDataBindingFinished(withSender Sender: Any)
    {
        if(Success == 0)
        {
            self.fetchingExpectation!.fulfill()
            Success = 1
        }
        
    }
    
    var fetchingExpectation:XCTestExpectation?
   
    init(WithExpectation Expectation:XCTestExpectation)
    {
        self.fetchingExpectation = Expectation
    }
    
}

class TestFetcherDelegate:DataFetcherDelegate
   {
        let fetchingExpectation:XCTestExpectation
        init(WithExpectation Expectation:XCTestExpectation)
        {
            self.fetchingExpectation = Expectation
        }
    
        var fetchedData:Any?
        func didFetchData(WithFetchedData DataFetched: Any?, WithError ErrorReported: Error?)
        {
           self.fetchingExpectation.fulfill()
           self.fetchedData = DataFetched
        }
    
        func identification()-> (String)
        {
        return self.fetchingExpectation.description
        }
       
       
   }

class TestDataBaseFetcherDelegate:NSObject, DataBaseRefreshedDelegate
{
     
    
    let fetchingExpectation:XCTestExpectation
    init(WithExpectation Expectation:XCTestExpectation)
    {
        self.fetchingExpectation = Expectation
    }
    
    
    func dataBaseDataReady(WithError ErrorReported: Error?)
    {
        self.fetchingExpectation.fulfill()
        
    }
    
    
}



