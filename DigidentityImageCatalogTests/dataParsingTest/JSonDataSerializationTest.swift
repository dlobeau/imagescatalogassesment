//
//  JSonDataSerializationTest.swift
//  DigidentityImageCatalogTests
//
//  Created by Didier Lobeau on 25/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import AwsomeDataFetcher

class JSonDataSerializationTest: XCTestCase
{
    let path:String = "1WcGvnm5.json"
    var parser:JsonParser?
    var coder:JsonStreamExtraction?
    var itemList:[CatalogItemGeneric]?
    var referenceData:Data?
    
    override func setUp()
    {
        let CurrenBundle = Bundle(for: type(of: self))
        let LocalJsonFile = LocalFileImp.init(WithBundle: CurrenBundle, WithURL:path)
              
        referenceData  = try! LocalJsonFile.fetchContent() as? Data
            
        parser = PinListJsonParserGeneric()
        coder = JsonStreamExtractionGeneric()
        
        let ParsingReturnValue:([CatalogItemGeneric]?,Error?) = parser!.parse(WithStream:referenceData!)
        itemList = ParsingReturnValue.0
        
    }

    override func tearDown() {
    }

   func testJSonDataSerialization_wheExtractingStream_DataExtractShouldBeEqualToReference()
    {
        let UnderTest = coder!.getStream(WithJSonObject: itemList)
    
        let ParsingReturnValue:([CatalogItemGeneric]?,Error?) = parser!.parse(WithStream:UnderTest.0!)
               
        let UnderTestList:[CatalogItemGeneric]? = ParsingReturnValue.0
        
        XCTAssertEqual(UnderTestList?.count, 10)
        
        var i = 0
        for CurentItem in UnderTestList!
        {
            XCTAssertEqual(CurentItem.id, itemList![i].id)
            XCTAssertEqual(CurentItem.confidence, itemList![i].confidence)
            XCTAssertEqual(CurentItem.text, itemList![i].text)
            XCTAssertEqual(CurentItem.image, itemList![i].image)
            
            i+=1
        }
        
        
       
        
    }

}
