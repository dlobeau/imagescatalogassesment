//
//  JsonDataParsingTest.swift
//  DigidentityImageCatalogTests
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import AwsomeDataFetcher

class JsonDataParsingTest: XCTestCase
{
    
    var itemList:[CatalogItemGeneric]?
    
    override func setUp()
    {
        let path:String = "1WcGvnm5.json"
        let CurrenBundle = Bundle(for: type(of: self))
        let LocalJsonFile = LocalFileImp.init(WithBundle: CurrenBundle, WithURL:path)
        let Content =  try! LocalJsonFile.fetchContent()
        let DataFromJson  = Content as! Data
            
        let parser = PinListJsonParserGeneric()
        let ParsingReturnValue:([CatalogItemGeneric]?,Error?) = parser.parse(WithStream:DataFromJson)
        itemList = ParsingReturnValue.0
    }

    override func tearDown()
    {
    }

    func testJsonDataParsing_whenParsingJsonStream_ItemsCountMustBe10()
     {
        XCTAssertEqual(itemList?.count, 10)
     }
    
    func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_IDShouldBe_4kQA1aQK8_Y()
    {
        let UnderTest = itemList?[0].id
        let Reference = "5e4eb391d491d"
        XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
    }
    
    func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_CreationDateShouldBe_2016_05_29T15_42_02_04_00()
    {
        let UnderTest = itemList?[0].text
        let Reference = "30. dilps"
        XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
    }
    
    func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_CreationWidthShouldBe_2448()
    {
        let UnderTest = itemList?[0].confidence
        let Reference:Float = 0.91
        XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
    }
    
    
    func testJsonDataParsing_whenParsingJsonStreamWithBadContent_ItemsCountMustBe0()
    {
        let path:String = "1WcGvnm5_badFormat.json"
        let CurrenBundle = Bundle(for: type(of: self))
        let LocalJsonFile = LocalFileImp.init(WithBundle: CurrenBundle, WithURL:path)
                  
        let DataFromJson  = try! LocalJsonFile.fetchContent() as! Data
                
        let parser = PinListJsonParserGeneric()
       
        let ParsingReturnValue:([CatalogItemGeneric]?,Error?) = parser.parse(WithStream:DataFromJson)
        itemList = ParsingReturnValue.0
        let Error = ParsingReturnValue.1
        XCTAssertNil(itemList)
        XCTAssertNotNil(Error)
        print(Error!.localizedDescription)
    }
     
     
}
