//
//  ApplicationDataBaseTest.swift
//  DigidentityImageCatalogTests
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class ApplicationDataBaseTest: XCTestCase
{
    var testEnvironment:TestEnvironment?
    var app:ApplicationManager?
    var pinList:[CatalogItem]?
    var dataBase:CatalogItemsDataBase?
    
    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "CatalogApplication")
        app = testEnvironment!.application as? ApplicationManager
        try! app?.resetCache()
        let expectation = self.expectation(description: "fetching_ApplicationDataBaseTest")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
       try! app?.loadData()
        
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        switch result
        {
            case .completed: print("Everything fulfilled")
            case .incorrectOrder: print("Unexpected order")
            case .timedOut: print("Everything did not fulfill")
            default: print("There was an issue")
        }
        dataBase = testEnvironment!.dataBase
    
        pinList = dataBase?.items()
    }

    override func tearDown()
    {
    }

    func testApplicationDataBase_DatabaseSHouldBeintializedFromApplication_shouldBeAccessible()
    {
        XCTAssertNotNil(dataBase);
    }

    func testApplicationDataBase_whenParsingJsonStream_10ItemsSHouldBeRetrived()
    {
        let UnderTest = pinList!.count
        let Reference = 10
        XCTAssertEqual(Reference,UnderTest)
    }
    
    
    
}
