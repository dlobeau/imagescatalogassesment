//
//  ApplicationManagerTest.swift
//  DigidentityImageCatalogTests
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework
import AwsomeDataFetcher

class ApplicationManagerTest: XCTestCase
{
    var testEnvironment:TestEnvironment?
    var app:ApplicationManager?
    
    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "CatalogApplication")
        app = testEnvironment!.application as? ApplicationManager
              
    }
    func startApplicationmanager()->()
    {
        let expectation = self.expectation(description: "fetching_ApplicationManagerTest")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
        try! app?.loadData()
        
        let result = XCTWaiter.wait(for: [expectation], timeout: 5.0, enforceOrder: true)
        switch result
        {
            case .completed: print("Everything fulfilled")
            case .incorrectOrder: print("Unexpected order")
            case .timedOut: print("Everything did not fulfill")
            default: print("There was an issue")
        }
    }
    
    override func tearDown()
    {
       
    }

   func testtestApplicationInitialization ()
     {
         startApplicationmanager()
        
         let Reference:String = "Application main file"
         let UnderTest:String? = app?.label()
         
         XCTAssertTrue(Reference == UnderTest);
     }
     
    func testApplicationInitialization_DataBaseExtraction()
     {
        startApplicationmanager()
        
        XCTAssertNotNil(app);
     }
    
   
    
    func testApplicationInitialization_CachedDataSHouldBeCreatedWhenNoStart()
    {
        startApplicationmanager()
        do
        {
            try app?.setCachedData(WithCacheSize:5)
        
            XCTAssertTrue(app!.isDataInCache());
        
       
            try app?.loadCacheInDataBase()
        }
        catch
        {
            XCTAssertTrue(false, error.localizedDescription)
        }
        
        let DataInCache =  app!.dataBase?.items()
        
        XCTAssertTrue(DataInCache?.count == 5);
    }
    
    func testApplicationInitialization_whenCachedDataExist_applicationShouldBeLoadedWithCache()
     {
        let path:String = "1WcGvnm5_half.json"
        let CurrenBundle = Bundle(for: type(of: self))
        let LocalJsonFile = LocalFileImp.init(WithBundle: CurrenBundle, WithURL:path)
               
        let DataFromJson  = try! LocalJsonFile.fetchContent() as! Data
             
        let parser = PinListJsonParserGeneric()
        let ParsingReturnValue:([CatalogItemGeneric]?,Error?) = parser.parse(WithStream:DataFromJson)
        let itemList:[CatalogItemGeneric] = ParsingReturnValue.0!
        
        try! app?.setCachedData(WithItems: itemList)
       
        app?.start()
        try! app?.loadData()
        let UnderTest:[CatalogItemGeneric] = app!.dataBase!.items()! as! [CatalogItemGeneric] 
        
         XCTAssertEqual(UnderTest.count, 6)
         
         var i = 0
         for CurentItem in UnderTest
         {
             XCTAssertEqual(CurentItem.id, itemList[i].id)
             XCTAssertEqual(CurentItem.confidence, itemList[i].confidence)
             XCTAssertEqual(CurentItem.text, itemList[i].text)
             XCTAssertEqual(CurentItem.image, itemList[i].image)
             
             i+=1
         }
         
         
     }

}
