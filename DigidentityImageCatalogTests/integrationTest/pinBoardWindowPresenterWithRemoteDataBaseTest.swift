//
//  pinBoardWindowPresenterWithRemoteDataBaseTest.swift
//  DigidentityImageCatalogTests
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class pinBoardWindowPresenterWithRemoteDataBaseTest: XCTestCase
{
    var testEnvironment:TestEnvironment?
    var app:ApplicationManager?
    var dataBase:CatalogItemsDataBase?
    var pinList:[CatalogItem]?
    
    

    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "CatalogApplication")
        app = testEnvironment!.application as? ApplicationManager
    }

    override func tearDown()
    {
        
    }

    func testpinBoardWindowPresenterWithRemoteDataBase_collectionInWiddowSouldBeInitalizeCorrectly()
    {
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
        try! app?.loadData()
       let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        switch result
        {
            case .completed: print("Everything fulfilled")
            case .incorrectOrder: print("Unexpected order")
            case .timedOut: print("Everything did not fulfill")
            default: print("There was an issue")
        }
        dataBase = testEnvironment!.dataBase
        
        let window = app?.window() as? CatalogItemPage
        window?.dataBinding()
        
        let CheckPageConformity = window!.testdelegate?()
        CheckPageConformity!.expectSender(window!, conformTo: dataBase!)
    }
    
    func testpinBoardWindowPresenterWithRemoteDataBase_ImageInCell0_mustBeConformToReference()
    {
        let expectation = self.expectation(description: "didfetchingDb")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
        try! app?.loadData()
        
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        switch result
        {
            case .completed: print("Everything fulfilled")
            case .incorrectOrder: print("Unexpected order")
            case .timedOut: print("Everything did not fulfill")
            default: print("There was an issue")
        }
                      
        dataBase = testEnvironment!.dataBase
        
        let window = app?.window() as? CatalogItemPage
        window?.dataBinding()
        
        let CellPresenter = window?.pinBoardCollection?.section(at: 0).cell(at: 0) as? CatalogItemsPageCollectionCell
        
        CellPresenter?.thumbnailImage?.dataBinding()
        let ImageUnderTest = CellPresenter?.thumbnailImage?.getImage?()
        XCTAssertEqual(ImageUnderTest!.pngData()?.count , 4512)
      
    }

  

}
