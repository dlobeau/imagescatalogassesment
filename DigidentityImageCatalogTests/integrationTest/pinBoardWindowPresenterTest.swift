//
//  pinBoardWindowPresenterTest.swift
//  DigidentityImageCatalogTests
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class pinBoardWindowPresenterTest: XCTestCase
{
    var testEnvironment:TestEnvironment?
    var app:ApplicationManager?
    var dataBase:CatalogItemsDataBase?
    var window:CatalogItemPage?
    
    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "CatalogApplication")
        app = testEnvironment!.application as? ApplicationManager
        dataBase = testEnvironment!.dataBase
        try! app?.resetCache()
        let expectation = self.expectation(description: "fetching_pinBoardWindowPresenterTest")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
        try! app?.loadData()
       let result = XCTWaiter.wait(for: [expectation], timeout:10.0, enforceOrder: true)
        switch result
        {
            case .completed: print("Everything fulfilled")
            case .incorrectOrder: print("Unexpected order")
            case .timedOut: print("Everything did not fulfill")
            default: print("There was an issue")
        }
        
        window = app?.window() as? CatalogItemPage
        window?.dataBinding()
    }

    override func tearDown() {
        
    }

    func testpinBoardWindowPresenter_collectionInWiddowSouldContain10Cell()
    {
        let Reference = 11
        let UnderTest = window!.pinBoardCollection!.section(at: 0).cells().count
        
        XCTAssert(Reference == UnderTest, "\(UnderTest) instead of \(Reference)")
    }
    
    func testpinBoardWindowPresenter_whenNextDataAreFetched_collectionInWiddowSouldContain20Cell()
    {
        let Cell = window!.pinBoardCollection!.section(at: 0).cell(at: 10)
        
        XCTAssert(Cell is CatalogItemsPageCollectionCellNextDataLoading)
        
        let expectation = self.expectation(description: "nextDataFetching")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        
        Cell.setAsynchronousDataBindingDelegate(currentDelegate)
        Cell.asynchronousDataBinding?()
        
        let result = XCTWaiter.wait(for: [expectation], timeout:10.0, enforceOrder: true)
        switch result
        {
            case .completed: print("Everything fulfilled")
            case .incorrectOrder: print("Unexpected order")
            case .timedOut: print("Everything did not fulfill")
            default: print("There was an issue")
        }
        
        window?.dataBinding()
        
        let Reference = 21
        let UnderTest = window!.pinBoardCollection!.section(at: 0).cells().count
        
        XCTAssertEqual(Reference,UnderTest)
        
        let CheckPageConformity = window!.testdelegate?()
        CheckPageConformity!.expectSender(window!, conformTo: dataBase!)
    }
    
    func testpinBoardWindowPresenter_collectionInWiddowSouldBeInitalizeCorrectly()
    {
        let CheckPageConformity = window!.testdelegate?()
        CheckPageConformity!.expectSender(window!, conformTo: dataBase!)
    }

    

}
