//
//  DataEncryptionTest.swift
//  DigidentityImageCatalogTests
//
//  Created by Didier Lobeau on 27/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import AwsomeDataFetcher

class DataEncryptionTest: XCTestCase {

    var encyptionDelegate:DataEncryptionDelegate?
    
    override func setUp()
    {
        let KeyFile = LocalFileImp.init(WithBundle: Bundle.main, WithURL: "EncriptionKey.pem")
        let IvFile = LocalFileImp.init(WithBundle: Bundle.main, WithURL: "EncryptionIv.pem")
        encyptionDelegate = AES256DataEncryption.init(WithKeyFile:KeyFile, WithIvFile:IvFile )
        
    }

    override func tearDown()
    {
        
    }

     func testLocalFile_dataEncrypt()
    {
           let ContentForFile = "un chasseur sachant chasser sans son chien est un bon chasseur"
           let DataToBeAdd =  ContentForFile.data(using: .utf8)
           
        do
        {
            let CryptedData = try self.encyptionDelegate!.encrypt(WithData: DataToBeAdd!)
         
            let UncryptedData = try self.encyptionDelegate!.decrypt(WithData: CryptedData)
            
            
            XCTAssertEqual(UncryptedData, DataToBeAdd)
            
       }
       catch
       {
           XCTAssertTrue(false, error.localizedDescription)
       }
    }
       
    func testLocalFile_dataDecrypt()
    {
           
           
    }


}
