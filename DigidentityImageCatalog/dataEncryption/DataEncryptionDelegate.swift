//
//  DataEncryption.swift
//  AwsomeDataFetcher
//
//  Created by Didier Lobeau on 27/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import AwsomeDataFetcher

protocol DataEncryptionDelegate
{
    func encrypt(WithData InputData:Data) throws ->(Data)
    func decrypt(WithData InputData:Data) throws ->(Data)
}

class AES256DataEncryption:DataEncryptionDelegate
{
    let encryptionDelegate = CommonCryptoDataEncryption.init()
    var keyFile:LocalFile
    var ivFile:LocalFile
    
    
    init(WithKeyFile KeyFile:LocalFile, WithIvFile IvFile:LocalFile)
    {
        self.keyFile = KeyFile
        self.ivFile = IvFile
        
    }
    
    func encrypt(WithData InputData: Data) throws -> (Data)
    {
        guard let key = try keyFile.fetchContent() as? Data,
                let Iv = try ivFile.fetchContent() as? Data
        else
        {
            throw ErrorEncryption.missingFile(descriptionText:"check key and IV encryption file")
            
        }
        let ciphertext = try encryptionDelegate.encrypt(data: InputData, key: key, iv: Iv)
        
        return ciphertext
    }
    
    func decrypt(WithData InputData: Data) throws -> (Data)
    {
        guard let key = try keyFile.fetchContent() as? Data
        else
        {
            throw ErrorEncryption.missingFile(descriptionText:"check key encryption file")
        }
        
        return try encryptionDelegate.decrypt(data: InputData, key: key)
    }
    
}

class NoEncryptionDataEncryption:DataEncryptionDelegate
{
    func encrypt(WithData InputData: Data) throws -> (Data)
    {
        return InputData
    }
    
    func decrypt(WithData InputData: Data) throws -> (Data)
    {
        return InputData
    }
}
