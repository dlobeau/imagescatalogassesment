//code from https://medium.com/@eneko/aes256-cbc-file-encryption-from-the-command-line-with-swift-cd1f88f2e1ec
//

import Foundation
import CommonCrypto

enum ErrorEncryption: Error
{
    case encryptionError(status: CCCryptorStatus)
    case decryptionError(status: CCCryptorStatus)
    case keyDerivationError(status: CCCryptorStatus)
    case missingFile(descriptionText: String)
}

struct CommonCryptoDataEncryption
{

    func encrypt(data: Data, key: Data, iv: Data) throws -> Data
    {
        // Output buffer (with padding)
        let outputLength = data.count + kCCBlockSizeAES128
        var outputBuffer = Array<UInt8>(repeating: 0,
                                        count: outputLength)
        var numBytesEncrypted = 0
        let status = CCCrypt(CCOperation(kCCEncrypt),
                             CCAlgorithm(kCCAlgorithmAES),
                             CCOptions(kCCOptionPKCS7Padding),
                             Array(key),
                             kCCKeySizeAES256,
                             Array(iv),
                             Array(data),
                             data.count,
                             &outputBuffer,
                             outputLength,
                             &numBytesEncrypted)
        guard status == kCCSuccess else {
            throw ErrorEncryption.encryptionError(status: status)
        }
        let outputBytes = iv + outputBuffer.prefix(numBytesEncrypted)
        return Data(outputBytes)
    }
    
    func decrypt(data cipherData: Data, key: Data) throws -> Data
    {
        // Split IV and cipher text
        let iv = cipherData.prefix(kCCBlockSizeAES128)
        let cipherTextBytes = cipherData
                               .suffix(from: kCCBlockSizeAES128)
        let cipherTextLength = cipherTextBytes.count
        // Output buffer
        var outputBuffer = Array<UInt8>(repeating: 0,
                                        count: cipherTextLength)
        var numBytesDecrypted = 0
        let status = CCCrypt(CCOperation(kCCDecrypt),
                             CCAlgorithm(kCCAlgorithmAES),
                             CCOptions(kCCOptionPKCS7Padding),
                             Array(key),
                             kCCKeySizeAES256,
                             Array(iv),
                             Array(cipherTextBytes),
                             cipherTextLength,
                             &outputBuffer,
                             cipherTextLength,
                             &numBytesDecrypted)
        guard status == kCCSuccess else {
            throw ErrorEncryption.decryptionError(status: status)
        }
        // Discard padding
        let outputBytes = outputBuffer.prefix(numBytesDecrypted)
        return Data(outputBytes)
    }
    
    
}
