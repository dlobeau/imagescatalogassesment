//
//  ImageCatalogError.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 27/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

public enum ImageCatalogError:Error
{
    case UnkownError( ErrorDescription:String)
    
    
}
