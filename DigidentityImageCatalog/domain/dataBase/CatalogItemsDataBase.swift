//
//  pinsJsonDataBase.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import AwsomeDataFetcher


protocol DataBaseRefreshedDelegate:NSObject
{
    func dataBaseDataReady(WithError ErrorReported:Error?)->()
}

protocol FetchDataAsynchronously
{
    func fetchData(WithDelegate Delegate:KAASynchronousDataBindingDelegate)->()
    func fetchNextData(WithDelegate Delegate:KAASynchronousDataBindingDelegate)->()
}

protocol ManageItemsData
{
    func addData(WithData NewData:Data) -> (Error?)
    func appendData(WithData NewDate:Data) -> (Error?)
    
    func addData(WithItemList List:[CatalogItemGeneric])
    func appendData(WithItemList List:[CatalogItemGeneric]) 
}

protocol CatalogItemsDataBase:KADomain,KASeriazableObject,FetchDataAsynchronously,ManageItemsData
{
    var dataBaseReadyDelegate:DataBaseRefreshedDelegate?{get set}
    var parser:JsonParser?{get set}
    var fetcher:RemoteDataFetcher?{get set}
     
    func items() -> ([CatalogItem]?)
    
}

class CatalogItemsDataBaseSerializable: KAGenericDomain,CatalogItemsDataBase
{
    
    // MARK: CatalogItemsDataBase protocol
    var parser: JsonParser?
    var fetcher: RemoteDataFetcher?
    private var lastFetchedData:[CatalogItemGeneric]?
    
    weak var dataBaseReadyDelegate: DataBaseRefreshedDelegate?
    var fetchDataDelegate:DataBaseFetcherDelegate?
    var fetchNextDataDelegate:DataBaseNextDataFetcherDelegate?
    
    func items() -> ([CatalogItem]?)
    {
        return lastFetchedData
    }
    
    // MARK: KADomain protocol
    
    static override func getTypeTag() -> String!
    {
        return "pinsJsonDataBase"
    }
    
    override func request(withID RequestID: String!) -> KADomain
    {
       var RequestedData:KADomain?
       
       switch RequestID
       {
            case "allPins":
                RequestedData = self;
            default:
                RequestedData = super.request(withID: RequestID)
       }
        
        
        let ReturnValue = RequestedData ?? KANullDomain.nullDomainObject()
       
        return ReturnValue
    }

    
    override func childs() -> [KADomain]!
    {
        return (self.items() ?? [KADomain]())
    }
    
    
}

extension CatalogItemsDataBaseSerializable:FetchDataAsynchronously
{
    func fetchData(WithDelegate Delegate:KAASynchronousDataBindingDelegate)->()
    {
        guard let Fetcher = self.fetcher
        else
        {
            return
        }
        
        self.fetchDataDelegate = DataBaseFetcherDelegate.init(WithDatabase: self, WithDelegate:Delegate)
        Fetcher.fetch(WithURL: self.label(),WithDelegate: self.fetchDataDelegate!)
    }
    
    func fetchNextData(WithDelegate Delegate:KAASynchronousDataBindingDelegate)
    {
        guard let Fetcher = self.fetcher
        else
        {
            return
        }
        if let lastItemId = self.lastFetchedData?.last?.id()
        {
            let LimitParameter = LimitRequestParameter(WithLimitLow: "", WithLimitHigh: lastItemId)
            self.fetchNextDataDelegate =  DataBaseNextDataFetcherDelegate.init(WithDatabase: self, WithDelegate:Delegate)
            Fetcher.fetch(WithURL: self.label(), WithDelegate: self.fetchNextDataDelegate!, WithLimits: LimitParameter)
        }
        else
        {
            Delegate.didASynchronousDataBindingFinished(withSender: self)
        }
    }
}

extension CatalogItemsDataBaseSerializable:ManageItemsData
{
    func addData(WithData NewData:Data) -> (Error?)
    {
        guard let Parser = self.parser
        else
        {
            return JsonParserError.noParser(description: "Parser not defined in dataBase!")
        }
        
        let ParsingREturnValue:([CatalogItemGeneric]?,Error?) = Parser.parse(WithStream:NewData)
        self.lastFetchedData = ParsingREturnValue.0
        return ParsingREturnValue.1
    }
    
    func appendData(WithData NewData:Data) -> (Error?)
    {
        guard let Parser = self.parser
        else
        {
            return JsonParserError.noParser(description: "Parser not defined in dataBase!")
        }
        
        let ParsingREturnValue:([CatalogItemGeneric]?,Error?) = Parser.parse(WithStream:NewData)
        let FetchedData:[CatalogItemGeneric] = ParsingREturnValue.0 ?? [CatalogItemGeneric]()
        self.lastFetchedData?.append(contentsOf: FetchedData)
       
        return ParsingREturnValue.1
    }
    func addData(WithItemList List:[CatalogItemGeneric])
    {
        self.lastFetchedData =  List
    }
    func appendData(WithItemList List:[CatalogItemGeneric])
    {
        self.lastFetchedData?.append(contentsOf: List)
    }
}
