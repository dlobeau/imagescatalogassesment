//
//  DataBaseAsynchronousFetchDataDelegate.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 28/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import AwsomeDataFetcher
import cleanProjectFramework

class DataBaseFetcherDelegate:DataFetcherDelegate
{
    unowned var catalogDb:CatalogItemsDataBaseSerializable
    unowned var delegate:KAASynchronousDataBindingDelegate
    
    init(WithDatabase DB:CatalogItemsDataBaseSerializable, WithDelegate Delegate:KAASynchronousDataBindingDelegate )
    {
        catalogDb = DB
        delegate = Delegate
    }
    
    func identification()->(String)
    {
        return catalogDb.id() + catalogDb.labelIdentifier() + "fetchData"
    }
    
    func didFetchData(WithFetchedData DataFetched: Any?, WithError ErrorReported: Error?)
    {
        var JsonParsingError:Error?
        
        if let  DataObject = DataFetched as? Data
        {
            JsonParsingError = catalogDb.addData(WithData: DataObject)
        }
        
        //send network error if one occured else send parsing error
        let ReturnedError = ErrorReported != nil ? ErrorReported : JsonParsingError
        
        delegate.didASynchronousDataBindingFinished(withSender: ReturnedError ?? self)
        catalogDb.dataBaseReadyDelegate?.dataBaseDataReady( WithError:ReturnedError)
    }
    
}

class DataBaseNextDataFetcherDelegate:DataBaseFetcherDelegate
{
    
    override func identification()->(String)
    {
        return catalogDb.id() + catalogDb.labelIdentifier() + "fetchNextData"
    }
    
    override func didFetchData(WithFetchedData DataFectched: Any?, WithError ErrorReported: Error?)
    {
        var JsonParsingError:Error?
        
        if let  DataObject = DataFectched as? Data
        {
            JsonParsingError = catalogDb.appendData(WithData: DataObject)
        }
       
        //send network error if one occured else send parsing error
        let ReturnedError = ErrorReported != nil ? ErrorReported : JsonParsingError
        
       delegate.didASynchronousDataBindingFinished(withSender: ReturnedError ?? self)
        catalogDb.dataBaseReadyDelegate?.dataBaseDataReady( WithError:ReturnedError)
    }
    
}

