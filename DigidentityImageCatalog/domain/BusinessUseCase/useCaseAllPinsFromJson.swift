//
//  useCaseAllPinsFromJson.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 07/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

import cleanProjectFramework


protocol QueryBusinessUseCaseAllPins:KAQueryBusinessUseCase,KASeriazableObject
{
}


class QueryBusinessUseCaseAllPinsFromJson:KAQueryBusinessUseCaseNoAction,QueryBusinessUseCaseAllPins
{
    static override func getTypeTag() -> String!
    {
        return "useCaseAllPinsFromJson"
    }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!)
    {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
           super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
    }
      
    
    //currently the request is doing nothing but returning the repository in parameter that is Catalog items database
    override func execute(withRepository Repository: KADomain!, withValue Value: KADomain!) -> KADomain! {
        super.execute(withRepository: Repository, withValue: Value)
    }
    
}
