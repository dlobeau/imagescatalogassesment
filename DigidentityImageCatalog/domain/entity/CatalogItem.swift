//
//  pin.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 04/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


protocol CatalogItem:KADomain,Codable
{
    var id:String {get}
    var image:String {get}
    var text:String{get}
    var confidence:Float{get}
    
    
}


class CatalogItemGeneric:KAGenericDomain, CatalogItem
{
    enum CodingKeys:String,CodingKey
    {
        case id = "_id"
        case image = "img"
        case text
        case confidence
    }
    
    var id:String
    var image:String
    var text:String
    var confidence:Float
    
    required init(from decoder:Decoder) throws
    {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        image = try values.decode(String.self, forKey: .image)
        text = try values.decode(String.self, forKey: .text)
        confidence = try values.decode(Float.self, forKey: .confidence)
      
        super.init(label: "", withLabelIdentifier: "", withObjectFamilyName: "pin", withID: id)
    }
    
    override func request(withID RequestID: String!) -> KADomain!
    {//used presenter is making data request
        var ReturnValue:KADomain?
        
        switch RequestID {
        case "thumbnail":
            ReturnValue = KAGenericDomain.createGenericObject(withLabel: self.image) as? KADomain
            ReturnValue?.setID("thumbnail")
        case "text":
            ReturnValue = KAGenericDomain.createGenericObject(withLabel: self.text) as? KADomain
            ReturnValue?.setID("text")
        case "confidence":
            let ConfidenceString =  String.init(self.confidence)
            ReturnValue = KAGenericDomain.createGenericObject(withLabel:ConfidenceString) as? KADomain
            ReturnValue?.setID("confidence")
        case "itemId":
            let ConfidenceString =  String.init(self.id())
            ReturnValue = KAGenericDomain.createGenericObject(withLabel:ConfidenceString) as? KADomain
            ReturnValue?.setID("itemId")
      
        default:
            ReturnValue = super.request(withID: RequestID)
        }
        return ReturnValue ?? KANullDomain.nullDomainObject()
    }


   
}





