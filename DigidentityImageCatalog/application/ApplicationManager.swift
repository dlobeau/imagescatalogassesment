//
//  ApplicationManagerWithRemoteData.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 11/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import AwsomeDataFetcher


    protocol ApplicationManager:KAApplication,KASeriazableObject,CacheDataManagement
    {
        var dataBase:CatalogItemsDataBase?{get}
        func loadData() throws
    }

    protocol CacheDataManagement
    {
        func setCachedData(WithCacheSize CacheSize:Int) throws
        func setCachedData(WithItems Items: [CatalogItem]) throws
        
        func isDataInCache()->(Bool)
        func loadCacheInDataBase() throws
        func resetCache() throws
    }

    protocol ApplicationConfigutationFileProvider
    {
        func certificationFile(WithDataBase DataBase:CatalogItemsDataBase )->(LocalFile?)
        func encryptionKeyFile() throws ->(LocalFile)
        func encryptionIvFile() throws ->(LocalFile)
        
    }

    class ApplicationManagerSerializable: KAApplicationSerializableObjectImp, ApplicationManager
    {
        
        private var dataBaseVar:CatalogItemsDataBase?
        private var encryptionDelegate:DataEncryptionDelegate?
        var dataBase:CatalogItemsDataBase?
       
        static override func getTypeTag() -> String!
        {
           return "mainApplicationWithRemoteData"
        }
      
        override func start()
        {
            guard let DataBaseExtracted = super.datBaseContener()?.request(withID: "jsonDataBase") as? CatalogItemsDataBase
            else
            {
                return
            }
            
            DataBaseExtracted.parser = PinListJsonParserGeneric()
            DataBaseExtracted.dataBaseReadyDelegate =  self
            self.dataBase = DataBaseExtracted
            self.dataBase?.fetcher = self.fetcher(DataBaseExtracted)
            
        }
        
        func loadData() throws
        {
            try self.setEncriptionDelegateFromSettings()
            
            if(self.isDataInCache())
            {
                try self.loadCacheInDataBase()
                self.dataBaseDataReady(WithError: nil)
            }
            else
            {//loading data from asynchronous request to Backend
                self.asynchronousDataBinding()
            }
        }
        
        func setEncriptionDelegateFromSettings() throws
        {
            let KeyFile = try self.encryptionKeyFile()
            let IvFile = try self.encryptionIvFile()
            self.encryptionDelegate = AES256DataEncryption.init(WithKeyFile:KeyFile , WithIvFile:IvFile )
        }
    
        func fetcher(_ DataBase:CatalogItemsDataBase)->(RemoteDataFetcher?)
        {
            var ReturnValue:RemoteDataFetcher?
            
            if let KeyNode = DataBase.request(withID: "key"),
                let CerFile = self.certificationFile(WithDataBase: DataBase)
            {
                ReturnValue =  RemoteDataFetcherAlamo.init(WithCertificationFile: CerFile, WithKey: KeyNode.label())
            }
            return ReturnValue
        }
        
        override func dataBinding() -> Bool
        {//data loading is exclusively asynchronous
            
            return true
        }
        
        override func asynchronousDataBinding() -> ()
        {
            guard let Database =  self.dataBase
            else
            {
                return
            }
           if let Delegate = self.asynchronousDataBindingDelegate()
            {
                Database.fetchData(WithDelegate: Delegate)
            }
        }
    
}

extension ApplicationManagerSerializable: DataBaseRefreshedDelegate
{
    func dataBaseDataReady( WithError ErrorReported: Error?)
    {
        if let Delegate = self.asynchronousDataBindingDelegate()
        {
            Delegate.didASynchronousDataBindingFinished(withSender: ErrorReported ?? self)
            super.dataBinding()
        }
        
    }
}

extension ApplicationManagerSerializable: CacheDataManagement
{
    func loadCacheInDataBase() throws
    {
        if let CachedData = try self.getCachedData(),
            let DataBase = self.dataBase
        {
            DataBase.addData(WithItemList: CachedData)
            
        }
    }
    
    func setCachedData(WithItems Items: [CatalogItem]) throws
    {
        guard let ItemsList = Items as? [CatalogItemGeneric]
        else
        {
            return
        }
        let coder = JsonStreamExtractionGeneric()
        let DataFromJson = coder.getStream(WithJSonObject: ItemsList)
        if let CacheData = DataFromJson.0
        {
            try self.serializeCacheStream(WithStream: CacheData)
        }
        if let ErrorFromJson =  DataFromJson.1
        {
            throw ErrorFromJson
        }
    }
    
    func resetCache() throws
    {
        if (self.cacheFile().isFileExist())
        {
            try self.cacheFile().cleanFileFromUserEnvironment()
        }
        
    }
    
    
    func cacheFile()->(LocalFile)
    {
        let F = LocalFileImp.init(WithBundle: KAApplicationSerializableObjectImp.getBundle()
        , WithURL:  "DigidentityAppcache.json")
       
        return F
    }
    
    func setCachedData(WithCacheSize CacheSize:Int) throws
    {
        guard let Source = self.dataBase?.items() as? [CatalogItemGeneric]
        else
        {
            return
        }
        let Size = CacheSize > Source.count ? Source.count:CacheSize
        let Items = Source[..<Size]
        try self.setCachedData(WithItems: Array(Items))
        
    }
    
    func serializeCacheStream(WithStream DataStream: Data) throws
    {
        guard let EncryptionDelegate = self.encryptionDelegate
        else
        {
            throw ErrorEncryption.missingFile(descriptionText: "Encrryption delegate should be defined before calling serializeCacheStream")
        }
         let CryptedData = try EncryptionDelegate.encrypt(WithData: DataStream)
         self.cacheFile().update(WithContent: CryptedData)
        
    }
    
    func getCachedData() throws ->([CatalogItemGeneric]?)
    {
        guard let EncryptionDelegate = self.encryptionDelegate
        else
        {
            throw ErrorEncryption.missingFile(descriptionText: "Encrryption delegate should be defined before calling getCachedData")
        }
        
        var ReturnValue:([CatalogItemGeneric]?,Error?)
        let F =  self.cacheFile()
        if (F.isFileExist())
        {
             let FetchedData = try F.fetchContent()
            if let DataFromJson  =  FetchedData as? Data
            {
                let parser = PinListJsonParserGeneric()
                let DecryptedData = try EncryptionDelegate.decrypt(WithData: DataFromJson)
                ReturnValue = parser.parse(WithStream: DecryptedData)
            }
        }
        return ReturnValue.0
     }
    
     func isDataInCache()->(Bool)
     {
        return self.cacheFile().isFileExist()
    }
    
    
}


extension ApplicationManagerSerializable: ApplicationConfigutationFileProvider
{
    func certificationFile(WithDataBase DataBase:CatalogItemsDataBase )->(LocalFile?)
    {
        var ReturnValue:LocalFile?
        
        if let CurrentBundle = KAApplicationSerializableObjectImp.getBundle(),
            let CertificateNode = DataBase.request(withID: "Filecertificate")
        {
            ReturnValue = LocalFileImp.init(WithBundle: CurrentBundle, WithURL:CertificateNode.label())
        }
        
        return ReturnValue
    }
    
    func encryptionKeyFile( ) throws ->(LocalFile)
    {
        var KeyFile:LocalFile?
        
        if let CurrentBundle = KAApplicationSerializableObjectImp.getBundle(),
            let Node = super.datBaseContener()?.request(withID: "keyFile")
        {
            KeyFile = LocalFileImp.init(WithBundle: CurrentBundle, WithURL:Node.label())
        }
        
        guard let ReturnValue =  KeyFile
        else
        {
            throw ErrorEncryption.missingFile(descriptionText:"encryption key file is missing")
        }
        
        return ReturnValue
    }
    
    func encryptionIvFile() throws ->(LocalFile)
    {
       var IvFile:LocalFile?
       
       if let CurrentBundle = KAApplicationSerializableObjectImp.getBundle(),
           let Node = super.datBaseContener()?.request(withID: "IvFile")
       {
           IvFile = LocalFileImp.init(WithBundle: CurrentBundle, WithURL:Node.label())
       }
       
        guard let ReturnValue =  IvFile
        else
        {
            throw ErrorEncryption.missingFile(descriptionText:"encryption Iv file is missing")
        }
       return ReturnValue
    }
}
