//
//  SwiftInjDependencyInjection.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 09/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import Swinject
import cleanProjectFramework

//Dependency injection contener use in app
class ContenerDI: NSObject, KAInjectionDependencyDelegate
{
     let container = Container()
    
    func injectedObject(withParameters Parameters: [AnyHashable : Any]) -> Any
    {
       
        var ReturnValue:KASeriazableObject?
               
               
               if let Name = Parameters["group_id"] as? String
               {
                   switch Name
                   {
                      
                    case ApplicationManagerSerializable.getTypeTag():
                    ReturnValue = container.resolve(ApplicationManager.self, name: ApplicationManagerSerializable.getTypeTag())
                            
                       case CatalogItemsDataBaseSerializable.getTypeTag():
                           ReturnValue = container.resolve(CatalogItemsDataBase.self, name: CatalogItemsDataBaseSerializable.getTypeTag())
                       
                    
                    case CatalogItemsPagePresenter.getTypeTag():
                                             ReturnValue = container.resolve(CatalogItemPage.self, name: CatalogItemsPagePresenter.getTypeTag())
                                       
                    
                    case CatalogItemsPageCollectionPresenter.getTypeTag():
                            ReturnValue = container.resolve(CatalogItemsPageCollection.self, name: CatalogItemsPageCollectionPresenter.getTypeTag())
                                       
                    
                    case CatalogItemsPageCollectionCellPresenter.getTypeTag():
                            ReturnValue = container.resolve(CatalogItemsPageCollectionCell.self, name: CatalogItemsPageCollectionCellPresenter.getTypeTag())
                    
                    case ImagePresenter.getTypeTag():
                            ReturnValue = container.resolve(Image.self, name: ImagePresenter.getTypeTag())
                    
                    case QueryBusinessUseCaseAllPinsFromJson.getTypeTag():
                    ReturnValue = container.resolve(QueryBusinessUseCaseAllPins.self, name: QueryBusinessUseCaseAllPinsFromJson.getTypeTag())
                    
                    case CatalogItemsPageCollectionSectionPresenter.getTypeTag():
                    ReturnValue = container.resolve(CatalogItemsPageCollectionSection.self, name: CatalogItemsPageCollectionSectionPresenter.getTypeTag())
                    
                    
                    
                    case CatalogItemsPageCollectionCellNextDataLoadingPresenter.getTypeTag():
                    ReturnValue = container.resolve(CatalogItemsPageCollectionCellNextDataLoading.self, name: CatalogItemsPageCollectionCellNextDataLoadingPresenter.getTypeTag())
                    
                                       
                       default:
                           ReturnValue = KAApplicationSerializableObjectImp.injectionDependencyDelegateDefaultItems()?.injectedObject(withParameters: Parameters) as? KASeriazableObject
                   }
                   ReturnValue?.setObjectFamilyName(Parameters["group_id"] as! String)
                   ReturnValue?.setID(Parameters["type_id"] as! String)
                   ReturnValue?.setLabel(Parameters["label"] as! String)
                   ReturnValue?.setLabelIdentifier(Parameters["identifier"] as! String)
                   
               }
               return ReturnValue!
    }
    
   
    
    override init()
    {
        super.init()
        setupContainers()
    }
    
    
    func setupContainers()->()
    {
        
        let Parameter:[String:String] = ["group_id":"","identifier":"","label":"","type_id":""]
           
        container.register(ApplicationManager.self, name: ApplicationManagerSerializable.getTypeTag(),
                              factory: { _ in ApplicationManagerSerializable(dictionary: Parameter) })
        
               
           container.register(CatalogItemsDataBase.self, name: CatalogItemsDataBaseSerializable.getTypeTag(),
                              factory: { _ in CatalogItemsDataBaseSerializable(dictionary: Parameter) })
        
        container.register(CatalogItemPage.self, name: CatalogItemsPagePresenter.getTypeTag(),
        factory: { _ in CatalogItemsPagePresenter(dictionary: Parameter) })
        
        container.register(CatalogItemsPageCollection.self, name: CatalogItemsPageCollectionPresenter.getTypeTag(),
        factory: { _ in CatalogItemsPageCollectionPresenter(dictionary: Parameter) })
        
        container.register(CatalogItemsPageCollectionCell.self, name: CatalogItemsPageCollectionCellPresenter.getTypeTag(),
        factory: { _ in CatalogItemsPageCollectionCellPresenter(dictionary: Parameter) })
        
        container.register(Image.self, name: ImagePresenter.getTypeTag(),
               factory: { _ in ImagePresenter(dictionary: Parameter) })
        
        container.register(QueryBusinessUseCaseAllPins.self, name: QueryBusinessUseCaseAllPinsFromJson.getTypeTag(),
        factory: { _ in QueryBusinessUseCaseAllPinsFromJson(dictionary: Parameter) })
        
        container.register(CatalogItemsPageCollectionSection.self, name: CatalogItemsPageCollectionSectionPresenter.getTypeTag(),
               factory: { _ in CatalogItemsPageCollectionSectionPresenter(dictionary: Parameter) })
              
        
        
        container.register(CatalogItemsPageCollectionCellNextDataLoading.self, name: CatalogItemsPageCollectionCellNextDataLoadingPresenter.getTypeTag(),
               factory: { _ in CatalogItemsPageCollectionCellNextDataLoadingPresenter(dictionary: Parameter) })
        
        
              
        
    }
    
    
    
}
