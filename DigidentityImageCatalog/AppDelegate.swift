//
//  AppDelegate.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 04/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import UIKit
import cleanProjectFramework



protocol WindowDiplayer
{
    func displayWindow()->(UIViewController?)
    func displayErrorWindow(WithError ErrorReported:Error)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
 
    var window: UIWindow?
    var coordinator:ApplicationManager?
    var mainController:UIViewController?
   
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // create app "Coordinator"
        KAApplicationSerializableObjectImp.createInstance(withFileName: "CatalogApplication",withDIDelegate: ContenerDI.init())
        guard let app = KAApplicationSerializableObjectImp.instance() as? ApplicationManager
        else
        {
            return false
        }
        
        app.setAsynchronousDataBindingDelegate(self)
        app.start();
        self.coordinator = app
        self.mainController = self.displayWindow()
        
        do
        {
            try app.loadData()
        }
        catch
        {
            self.displayErrorWindow(WithError: error)
        }
       
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication)
    {
        guard let Coordinator = self.coordinator
        else
        {
            return
        }
         
        do
        {
            try Coordinator.resetCache()
            try Coordinator.setCachedData(WithCacheSize:10)
        }
        catch
        {
            self.displayErrorWindow(WithError: error)
        }
           
    }
    
    
}

extension AppDelegate:KAASynchronousDataBindingDelegate
{
    func didASynchronousDataBindingFinished(withSender Sender: Any)
    {//when data are fetched from backend, binds data and prepare UI for display
        guard  let Controller = mainController as? KAView,
        let Window = self.coordinator!.window()
        else
        {
            return
        }
        
        if let ErrorReported = Sender as? Error
        {
            self.displayErrorWindow(WithError: ErrorReported)
        }
        else
        {
            Window.dataBinding()
            Controller.setContent()
        }
     }
    
}

extension AppDelegate:WindowDiplayer
{
    func displayWindow()->(UIViewController?)
    {
        var ReturnValue:UIViewController?
        guard let WindowPresenter = self.coordinator!.window()
        else
        {
            return nil
        }
        
        let WindowViewController = WindowPresenter.createView(withOwner: nil) as? UIViewController
           
        switch WindowViewController
        {
           case .some(_):
               self.window = UIWindow.init(frame: UIScreen.main.bounds)
               self.window?.rootViewController = WindowViewController
               self.window?.makeKeyAndVisible()
               ReturnValue = WindowViewController
               
           case .none:
               ReturnValue = nil
        }
        
        return ReturnValue
    }
      
    func displayErrorWindow(WithError ErrorReported:Error)
    {
       let alert = UIAlertController(title: "Data loading error", message: ErrorReported.localizedDescription, preferredStyle: UIAlertController.Style.alert)
       alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
       self.mainController!.present(alert, animated: true, completion: nil)
    }
}


