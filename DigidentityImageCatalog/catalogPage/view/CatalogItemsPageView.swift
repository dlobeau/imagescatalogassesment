//
//  pinboardPageView.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


protocol DataLoadedDelegate:NSObject
{
    func dataLoadedInView(WithView View:KAView)->()
}

class CatalogItemsPageView:TLUIFacadeViewController
{
    @IBOutlet weak var collectionView: CatalogItemsPageCollectionView!
    
    override func viewDidLoad()
    {
        collectionView.dataSource = self
        collectionView.delegate = self
        let Layout = collectionView.collectionViewLayout as? PinterestLayout
        Layout?.delegate = self
    }
     
    override func setContent()
    {
        if let Title =  self.interface()?.getText?()
        {
            self.title = Title
        }
        
        self.collectionView.setContent()
    }
    
}

//MARK: DataLoadedDelegate protocole

extension CatalogItemsPageView:DataLoadedDelegate
{
    func dataLoadedInView(WithView View:KAView)->()
    {
        let Layout =  self.collectionView.collectionViewLayout as? PinterestLayout
        Layout?.refresh()
        Layout?.invalidateLayout()
    }
}

//MARK: UICollectionViewDelegate,UICollectionViewDataSource protocole

extension CatalogItemsPageView:UICollectionViewDelegate,UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        let CollectionPresenter = self.collectionView.interface() as? CatalogItemsPageCollection
        return CollectionPresenter?.sections().count ?? 0;
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        let CollectionPresenter = self.collectionView.interface() as? CatalogItemsPageCollection
        return CollectionPresenter?.section(at: section).cells().count ?? 0 ;
    }
   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if let Cell = cell as? CatalogCellView
        {
           Cell.delegateDataLoaded = self
        
           Cell.setContent()
        }
    }
       
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
           let CollectionPresenter = self.collectionView.interface() as? CatalogItemsPageCollection
           let  CellPresenter = CollectionPresenter!.section(at: indexPath.section).cell(at: indexPath.row) ;
           
           let CellView = CellPresenter.createView(withOwner: self.collectionView) as! CatalogCellView
        
        if(CellView.cellIndex != indexPath.row)
        {
             CellPresenter.setIsVisible?(true)
            CellView.cellIndex = indexPath.row
        }
           
        
           return CellView
    }
}

//MARK: PinterestLayoutDelegate

extension CatalogItemsPageView: PinterestLayoutDelegate
{
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        let CollectionPresenter = self.collectionView.interface() as? CatalogItemsPageCollection
        
        let  CellPresenter = CollectionPresenter!.section(at: indexPath.section).cell(at: indexPath.row) as? CatalogItemsPageCollectionCell ;
        
        let Height:CGFloat =  CellPresenter?.height?() ?? 300
        
        return Height
    }
    
}
