//
//  pinboardCellImageView.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

class CatalogItemsPageCollectionCellImageView: TLUIImageViewFacade
{
    
    weak var delegateDataLoaded:DataLoadedDelegate?
    override func setContent()
    {
        let Presenter = self.interface() as? ImagePresenter
        let Value = Presenter?.dataBinding() ?? false
        if (Value)
        {
            self.image = Presenter?.image
        }
        delegateDataLoaded?.dataLoadedInView(WithView:self)
    }
}
