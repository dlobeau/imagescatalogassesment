//
//  pinBoardCollectionView.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


class CatalogItemsPageCollectionView:TLUICollectionViewFacade
{
    lazy var refresher:UIRefreshControl =
        {
       
        let ReturnValue = UIRefreshControl()
        ReturnValue.tintColor = .gray
            ReturnValue.addTarget(self, action: #selector(refreshData), for: .valueChanged)
            return ReturnValue
        
    }()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.refreshControl = refresher
    }
    
    override func setContent()
    {
        self.reloadData()
        self.refresher.endRefreshing()
        
    }
    
    @objc func refreshData()->()
    {
        self.interface()?.setAsynchronousDataBindingDelegate(self)
        self.interface()?.asynchronousDataBinding?()
    }
    
    
}


extension CatalogItemsPageCollectionView: KAASynchronousDataBindingDelegate
{
    func didASynchronousDataBindingFinished(withSender Sender: Any)
    {
        self.setContent()
    }
    
    
}


