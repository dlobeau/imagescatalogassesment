//
//  PinBoardCellLoadingData.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 26/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

class CatalogItemsPageCollectionDataLoadingMarkerCell:CatalogCellView
{
   
    @IBOutlet weak var cellContentView: UIView!
    
    var activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        self.cellContentView.addSubview(activityIndicator)
        
        self.displayCatalogCell()
            
        activityIndicator.centerXAnchor.constraint(equalTo: self.cellContentView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.cellContentView.centerYAnchor).isActive = true
    } 
    
    func displayCatalogCell()
    {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        self.contentView.backgroundColor = .blue
        activityIndicator.hidesWhenStopped = true
    }
    
    func hideCatalogCell()
    {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        self.contentView.backgroundColor = .gray
        if let Presenter = self.interface()
        {
            Presenter.setIsVisible?(false)
        }
        
    }
    
    override func setContent()
    {
        
        guard let Presenter = self.interface() as? CatalogItemsPageCollectionCellNextDataLoading
        else
        {
            return
        }
        if (Presenter.isCellVisible)
        {
            self.contentView.backgroundColor = .blue
            activityIndicator.isHidden = false
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
       
            Presenter.setAsynchronousDataBindingDelegate(self)
            Presenter.asynchronousDataBinding!()
        }
    }
}

extension CatalogItemsPageCollectionDataLoadingMarkerCell:KAASynchronousDataBindingDelegate
{
    func didASynchronousDataBindingFinished(withSender Sender: Any)
    {
        self.delegateDataLoaded?.dataLoadedInView(WithView: self)
        
        self.hideCatalogCell()
        
    }
    
    
}

