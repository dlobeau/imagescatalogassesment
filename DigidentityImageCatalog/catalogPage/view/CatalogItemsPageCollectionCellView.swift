//
//  pinboardCellView.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


class CatalogCellView:TLUICollectionViewCellFacade
{
    weak var delegateDataLoaded:DataLoadedDelegate?
    var cellIndex:Int = -1
}


class CatalogItemsPageCollectionCellView:CatalogCellView,DataLoadedDelegate
{
    @IBOutlet weak var imageContener: CatalogItemsPageCollectionCellImageView!
    @IBOutlet weak var descriptionLabel: TLUILabelFacade!
    @IBOutlet weak var confidenceLabel: TLUILabelFacade!
    @IBOutlet weak var idLabel: TLUILabelFacade!
   
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.imageContener.delegateDataLoaded = self
    }
    
    func dataLoadedInView(WithView View:KAView)->()
    {
        self.delegateDataLoaded?.dataLoadedInView(WithView: View)
    }
    
    override func setContent()
    {
        imageContener.setContent()
        descriptionLabel.setContent()
        confidenceLabel.setContent()
        idLabel.setContent()
    }
    
}
