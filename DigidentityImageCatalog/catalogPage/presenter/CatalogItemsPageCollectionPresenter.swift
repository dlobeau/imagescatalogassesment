//
//  PinBoardPresenter.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


protocol CatalogItemsPageCollection:KATable,KASeriazableObject
{
    
}

class CatalogItemsPageCollectionPresenter:KATablePresenter,CatalogItemsPageCollection
{
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(PinBoardCellsCollectionPresenterConformToDomain.init())
        self.setViewFactoryDelegate(UIFactoryCreateCollection.init())
       
    }
    
    static override func getTypeTag() -> String! {
        return "catalogItemsPageCollection"
    }
    
    override func asynchronousDataBinding()
    {//make asynchronous request to refresh database with most recent 10 items from backend
        
        if let Application = KAApplicationSerializableObjectImp.instance() as? ApplicationManager,
        let DataBase = Application.dataBase
        {
            DataBase.fetchData(WithDelegate: self.asynchronousDataBindingDelegate())
        }
    }
    
}



class PinBoardCellsCollectionPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let Table = Sender as! CatalogItemsPageCollection
        let DataBase = Domain as! CatalogItemsDataBase
        
        let pinList =  DataBase.items()
        let Cells = Table.section(at: 0).cells()
        
        assert(pinList!.count + 1 == Cells.count, "Cells number and pins number not equal, \(pinList!.count), and \( Cells.count )")
        
        var i = 0;
        for currentPin in pinList!
        {
            let currentPinCell = Cells[i] as! CatalogItemsPageCollectionCell
            
            i+=1
           
            let Delegate = currentPinCell.testdelegate?()
            Delegate?.expectSender(currentPinCell, conformTo: currentPin)
         }
    }
}
