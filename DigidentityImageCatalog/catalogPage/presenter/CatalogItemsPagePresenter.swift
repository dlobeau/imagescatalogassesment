//
//  pinBoardWindow.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol CatalogItemPage:KAPresenter,KASeriazableObject
{
    var pinBoardCollection:CatalogItemsPageCollection? { get }
  
}

class CatalogItemsPagePresenter: KAGenericPresenter,CatalogItemPage
{
    var pinBoardCollection:CatalogItemsPageCollection?
    {
        return self.getChildwithIdentifier("collection") as? CatalogItemsPageCollection
    }
    
    static override func getTypeTag() -> String! {
           return "catalogItemPage"
       }
    
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(PinBoardWindowPresenterConformToDomain.init())
        self.setViewFactoryDelegate(KACreateWindowFactory.init())
    }
    
}

class PinBoardWindowPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let Page = Sender as! CatalogItemPage
        let DataBase = Domain as! CatalogItemsDataBase
        
         
        let Delegate = Page.pinBoardCollection!.testdelegate?()
        
        Delegate?.expectSender(Page.pinBoardCollection!, conformTo: DataBase)
    }
}
