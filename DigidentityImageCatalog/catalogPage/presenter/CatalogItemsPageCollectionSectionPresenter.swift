//
//  PinBoardCollectionSection.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 07/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol CatalogItemsPageCollectionSection:KASection,KASeriazableObject
{
    
}

class CatalogItemsPageCollectionSectionPresenter: KASectionPresenter,CatalogItemsPageCollectionSection
{
    static override func getTypeTag() -> String!
    {
           return "catalogItemsPageCollectionSection"
       }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        
    }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
           super.init(dictionary: Dictionary)
       }
}
