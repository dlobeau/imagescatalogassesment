//
//  CatalogItemsPageCollectionCellNextDataLoading.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 25/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import UIKit
import cleanProjectFramework
import AwsomeDataFetcher

protocol CatalogItemsPageCollectionCellNextDataLoading:KACell,KASeriazableObject
{
    var isCellVisible:Bool {get set}
}

class CatalogItemsPageCollectionCellNextDataLoadingPresenter:KACellPresenter,CatalogItemsPageCollectionCellNextDataLoading
{
    var isCellVisible = true
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setViewFactoryDelegate(UIFactoryCreateCollectionCell.init() )
        
    }
    
    override func isVisible() -> Bool {
        return isCellVisible
    }
    
    override func setIsVisible(_ isVisible: Bool) {
        isCellVisible = isVisible
    }
    
    static override func getTypeTag() -> String! {
        return "catalogItemsPageCollectionCellNextDataLoading"
    }
    
    override func height() -> CGFloat {
        return 300
    }
    
    override func asynchronousDataBinding()
    {//make asynchronous request to add next 10 items from backend
        
        if let Application = KAApplicationSerializableObjectImp.instance() as? ApplicationManager,
            let DataBase = Application.dataBase,
            let Delegate = self.asynchronousDataBindingDelegate()
        {
            DataBase.fetchNextData(WithDelegate:Delegate )
        }
    }
    
    override func dataBinding() -> Bool {
        return false
    }
    
}




