//
//  PinPresenter.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import UIKit
import cleanProjectFramework

protocol CatalogItemsPageCollectionCell:KACell,KASeriazableObject
{
    var thumbnailImage:Image?{get}
    var textLabel:KALabel?{get}
    var confidenceLabel:KALabel?{get}
    var idLabel:KALabel?{get}
    
     var isCellVisible:Bool {get set}
}

class CatalogItemsPageCollectionCellPresenter:KACellPresenter,CatalogItemsPageCollectionCell
{
    var isCellVisible = true
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(PinCellPresenterConformToDomain.init())
        self.setViewFactoryDelegate(UIFactoryCreateCollectionCell.init() )
    }
    
    override func height() -> (CGFloat)
    {
        
        return 300
    }
    
    override func isVisible() -> Bool {
        return isCellVisible
    }
    
    override func setIsVisible(_ isVisible: Bool) {
           isCellVisible = isVisible
       }
    
    static override func getTypeTag() -> String! {
        return "catalogItemsPageCollectionCell"
    }
    
    var thumbnailImage:Image?
    {
        return self.getChildwithIdentifier("image") as? Image
    }
    var textLabel:KALabel?
    {
        return self.getChildwithIdentifier("text") as? KALabel
    }
    var confidenceLabel:KALabel?
    {
        return self.getChildwithIdentifier("confidence") as? KALabel
    }
    var idLabel:KALabel?
    {
        return self.getChildwithIdentifier("itemId") as? KALabel
    }
    
    
}
class PinCellPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let CurrentCell = Sender as! CatalogItemsPageCollectionCell
        let PinData = Domain as! CatalogItem

        let DelegateTestImage = CurrentCell.thumbnailImage?.testdelegate?()
        DelegateTestImage?.expectSender!(CurrentCell.thumbnailImage!, conformToText:PinData.image)
        
        let DelegateTestConfidence = CurrentCell.confidenceLabel?.testdelegate?()
        DelegateTestConfidence?.expectSender!(CurrentCell.confidenceLabel!, conformToText:String.init(PinData.confidence))
        
        let DelegateTestText = CurrentCell.textLabel?.testdelegate?()
        DelegateTestText?.expectSender!(CurrentCell.textLabel!, conformToText:PinData.text)
        
        let DelegateIdText = CurrentCell.idLabel?.testdelegate?()
        DelegateIdText?.expectSender!(CurrentCell.idLabel!, conformToText:PinData.id())
    }
    
}
