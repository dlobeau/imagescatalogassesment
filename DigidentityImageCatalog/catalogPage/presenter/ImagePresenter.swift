//
//  PinThumbnailImage.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 07/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import UIKit
import cleanProjectFramework
import AwsomeDataFetcher



protocol Image:KAPresenter,KASeriazableObject
{
   
    
}

class ImagePresenter:KAGenericPresenter,Image
{
    
    var  image:UIImage?
    
    lazy var NO_IMAGE:UIImage =
        {
            return UIImage.init()
    }()
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(ImagePresenterConformToDomain.init())
        
    }
    
    static override func getTypeTag() -> String! {
        return "thumbnail"
    }
    
    override func dataBinding() -> Bool {
        let ReturnValue = super.dataBinding()
        
        let decodedData = Data(base64Encoded: self.getText(), options: [])
        if let data = decodedData
        {
            self.image = UIImage(data: data)
        }
        
        return ReturnValue
    }
    
    override func height() -> (CGFloat)
    {
       return self.image?.size.height ?? 0
    }
   
    override func getImage() -> UIImage!
    {
        return self.image ?? self.NO_IMAGE
    }
     
    
    
}


class ImagePresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo PathImageDomain: KADomain)
    {
        let CurrentImage = Sender as! Image
      
        
         CurrentImage.dataBinding()
     
        let UnderTest = CurrentImage.getText?()
        let Reference =  PathImageDomain.labelIdentifier()
        
        assert( UnderTest == Reference , "image not equal" )
    }
    
    func expectSender(_ Sender: KAPresenter, conformToText ReferenceText: String)
    {
        let CurrentImage = Sender as! Image
         
        CurrentImage.asynchronousDataBinding?()
            
        let UnderTest = CurrentImage.getText?()
         assert( UnderTest == ReferenceText , "image not equal" )
    }
    
}

