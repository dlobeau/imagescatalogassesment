//
//  PinListJsonParser.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 04/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

enum JsonParserError: Error
{
    case noParser(description:String)
}

protocol JsonParser
{
     func parse<T:Decodable>(WithStream Stream: Data) -> (T?,Error?)
}


struct PinListJsonParserGeneric:JsonParser
{
    func parse<T:Decodable>(WithStream Stream: Data) -> (T?,Error?)
    {
        var ReturnData:T?
        
        do
        {
            ReturnData = try JSONDecoder().decode(T.self, from: Stream)
         }
        catch let parsingError
        {
            return (nil,parsingError)
        }
        return (ReturnData,nil)
    }
    
    
   
    
    
}

