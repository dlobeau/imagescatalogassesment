//
//  JsonSerialization.swift
//  DigidentityImageCatalog
//
//  Created by Didier Lobeau on 25/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol JsonStreamExtraction
{
     func getStream<T:Codable>(WithJSonObject JSonObject: T) -> (Data?,Error?)
}


struct JsonStreamExtractionGeneric:JsonStreamExtraction
{
    func getStream<T:Codable>(WithJSonObject JSonObject: T) -> (Data?,Error?)
    {
        var ReturnValue:Data?
        
        do
        {
            ReturnValue = try JSONEncoder().encode(JSonObject)
         }
        catch let parsingError
        {
            return (nil,parsingError)
        }
        return (ReturnValue,nil)
    }
    
    
   
    
    
}
